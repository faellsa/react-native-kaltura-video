require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name         = "react-native-kaltura"
  s.version      = "1.0.0"
  s.summary      = "Video player using Kaltura SDK"
  s.description  = <<-DESC
              Video player using Kaltura SDK
                   DESC
  s.homepage     = "https://github.com/github_account/react-native-kaltura"
  s.license      = "MIT"
  s.license    = { :type => "MIT" }
  s.authors      = { "HnrqSsS" => "hnrq.assuncao@gmail.com" }
  s.platforms    = { :ios => "9.0" }
  s.source       = { :git => "https://github.com/hnrqsss/react-native-kaltura.git" }
  s.cocoapods_version      = ">= 1.6.1"

  s.source_files = "ios/**/*.{h,m,swift}"
  s.requires_arc = true

  s.swift_version = "4.2"
  s.dependency "React"

  s.dependency "PlayKit"
  s.dependency "PlayKitProviders"
  s.dependency "PlayKit_IMA"
  s.dependency "DownloadToGo", "~> 3.11.0"
  s.dependency "Realm", "~> 3.21.0"
  s.dependency "RealmSwift", "~> 3.21.0"
  s.dependency "ObjcExceptionBridging", "~> 1.0.1"

  s.pod_target_xcconfig = {
    "SWIFT_VERSION" => "5.0",
    "VALID_ARCHS" => "x86_64 arm64",
    "DEFINES_MODULE" => "true",
    "CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES" => "YES"
  }
end