# react-native-kaltura (iOS only)

## Install

`$ yarn add https://hnrqassuncao@bitbucket.org/hnrqassuncao/react-native-kaltura.git`

#### iOS

add `use_frameworks!` at the first line of your Podfile 

#### RN >= 0.60

`$ cd ios/ && pod install && cd ..`

#### RN <= 0.59 ( I didn't test it yet )
`$ react-native link react-native-kaltura`
`$ cd ios/ && pod install && cd ..`

#### Android

## Usage
```javascript
import Kaltura from 'react-native-kaltura'

const examplePartnerId = '00000000'
const exampleEntryId = 'example_00'
const exampleKS = 'example ks'

function App() {
    ...
    const kRef = useRef(null)
    ...
    return (
        ...
        <Kaltura
            partnerId={examplePartnerId}
            entryId={exampleEntryId}
            ks={exampleKS}
            ref={kRef}
        >
        ...
    )
}

...

```

## Props

| props     | types  | description        |
|-----------|--------|--------------------|
| ks        | String | Kaltura Session    |
| partnerId | Number | Kaltura Partner_id |
| entryId   | String | Kaltura entry_id   |


## Functions


## License
MIT

## Contributors

