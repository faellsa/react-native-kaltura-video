import React, { Component } from 'react';
import { requireNativeComponent, ViewPropTypes, View, Text, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';


export default class KalturaVideo extends Component{

    state = { loading: true, isPlay: false };

    _onVideoLoad = (event) => {
        this.setState({ loading: false })
        console.log('opa', event.nativeEvent)
    };

    togglePlay(isPlay){
        this.setState({ isPlay });
        this.player.setNativeProps({paused: isPlay});
    }

    render(){
        const nativeProps = Object.assign({}, this.props);
        Object.assign(nativeProps, {
            style: [nativeProps.style],
        });
        const { loading, isPlay } = this.state;

        return(
            <View style={{...this.props.style}}>
                <RNKaluraVideo ref={(ref) => this.player = ref} {...nativeProps} onVideoLoad={this._onVideoLoad}>
                    <View style={{flex: 1}}>
                        {   loading &&
                            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                <Text style={{color: 'white'}}>Loading...</Text>
                            </View>
                        }
                    </View>
                </RNKaluraVideo>
                <View style={{position: 'absolute', bottom: 0, height: 60, width: '100%', justifyContent: 'center', alignItems: 'flex-start', backgroundColor: 'transparent'}}>
                    <TouchableOpacity style={{width: 50, height: '100%', backgroundColor: 'white', justifyContent: 'center', alignItems: 'center'}} onPress={() => this.togglePlay(!isPlay)}>
                        <Text>{isPlay ? 'Pause': 'Play'}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

KalturaVideo.propTypes = {
    onVideoLoad: PropTypes.func,
};

let RNKaluraVideo = requireNativeComponent('KalturaView');



