package com.reactlibrary;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.ThemedReactContext;

import com.facebook.react.uimanager.events.RCTEventEmitter;
import com.kaltura.playkit.PKLog;
import com.kaltura.playkit.PlayerEvent;
import com.kaltura.tvplayer.KalturaOvpPlayer;
import com.kaltura.tvplayer.KalturaPlayer;
import com.kaltura.tvplayer.OVPMediaOptions;
import com.kaltura.tvplayer.PlayerInitOptions;

import android.annotation.SuppressLint;
import android.widget.FrameLayout;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.graphics.Color;


public class KalturaView extends FrameLayout {

    //CONSTANTS
    public static final String TAG = "KalturaView";
    public static final int PARTNER_ID = 2215841;
    public static final String ENTRY_ID = "1_w9zx2eti";
    public static final String SERVER_URL = "https://cdnapisec.kaltura.com";
    private static final PKLog log = PKLog.get("MainActivity");

    public KalturaPlayer player;
    private ViewGroup.LayoutParams layoutParams;
    private ViewGroup container;
    private ThemedReactContext context;
    private RCTEventEmitter eventEmitter;

    //States
    private boolean mPaused = false;

    public enum Events {
        EVENT_LOAD("onVideoLoad"),
        EVENT_FULLSCREEN_DID_DISMISS("onVideoFullscreenPlayerDidDismiss");

        private final String mName;

        Events(final String name) {
            mName = name;
        }

        @Override
        public String toString() {
            return mName;
        }
    }

    public KalturaView(ThemedReactContext themedReactContext) {
        super(themedReactContext);

        this.context = themedReactContext;
        eventEmitter = themedReactContext.getJSModule(RCTEventEmitter.class);
        layoutParams = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);

        this.setBackgroundColor(Color.BLACK);

        this.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        this.loadPlaykitPlayer();
    }

    public void loadPlaykitPlayer() {
        PlayerInitOptions playerInitOptions = new PlayerInitOptions(PARTNER_ID);

        this.player = KalturaOvpPlayer.create(this.context, playerInitOptions);
        this.player.setAutoPlay(false);
        this.player.setPlayerView(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        container = new LinearLayout(this.context);
        container.addView(this.player.getPlayerView());
        addViewInLayout(container, 0, layoutParams);

        OVPMediaOptions ovpMediaOptions = buildOvpMediaOptions(ENTRY_ID, null);
        player.loadMedia(ovpMediaOptions, (entry, loadError) -> {
            if (loadError != null) {
                log.d(TAG, loadError.getMessage());
            } else {
                log.d(TAG, "LOADED");
            }
        });

        addPlayerStateListener();
    }

    private void addPlayerStateListener() {
        this.invalidate();
        this.player.addListener(this, PlayerEvent.canPlay, event -> {
            log.d(TAG, "Player can play");
            WritableMap loadEvent = Arguments.createMap();
            loadEvent.putBoolean(String.valueOf(Events.EVENT_LOAD), true);
            eventEmitter.receiveEvent(getId(), Events.EVENT_LOAD.toString(), loadEvent);
        });

        this.player.addListener(this, PlayerEvent.playing, event -> {
            log.d(TAG, "playing");
            this.invalidate();
        });
    }

    private OVPMediaOptions buildOvpMediaOptions(String entry_id, String ks) {
        OVPMediaOptions ovpMediaOptions = new OVPMediaOptions();
        ovpMediaOptions.entryId = entry_id;
        ovpMediaOptions.ks = ks;
        ovpMediaOptions.startPosition = 0L;

        return ovpMediaOptions;
    }

    @Override
    @SuppressLint("DrawAllocation")
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if (!changed) {
            return;
        }

    }
    // FUNCTIONS
    public void setPausedModifier(final boolean paused) {
        mPaused = paused;
        if (mPaused) {
            if (player.isPlaying()) {
                player.pause();
            }
        } else {
            if (!player.isPlaying()) {
                player.play();
            }
        }
    }
}
