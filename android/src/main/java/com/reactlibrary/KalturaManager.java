package com.reactlibrary;

import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.ViewGroupManager;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.ThemedReactContext;

import java.util.Map;

import javax.annotation.Nullable;

public class KalturaManager extends ViewGroupManager<KalturaView> {

    public static final String REACT_CLASS = "KalturaView";
    public static final String PROP_PAUSED = "paused";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    public KalturaView createViewInstance(ThemedReactContext reactContext) {
        return new KalturaView(reactContext);
    }

    @Override
    @Nullable
    public Map getExportedCustomDirectEventTypeConstants() {
        MapBuilder.Builder builder = MapBuilder.builder();
        for (KalturaView.Events event : KalturaView.Events.values()) {
            builder.put(event.toString(), MapBuilder.of("registrationName", event.toString()));
        }
        return builder.build();
    }

    @Override
    public boolean needsCustomLayoutForChildren() {
        return true;
    }

    @ReactProp(name = PROP_PAUSED, defaultBoolean = false)
    public void setPaused(final KalturaView videoView, final boolean paused) {
        videoView.setPausedModifier(paused);
    }
}
