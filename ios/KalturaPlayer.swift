//
//  KalturaPlayer.swift
//  Kaltura
//
//  Created by Fortal Sistemas on 23/01/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

import UIKit
import PlayKit
import PlayKit_IMA
import PlayKitUtils
import PlayKitProviders


@objc(KalturaPlayer)
class KalturaPlayer: UIView {
 var player: Player!
 var playheadTimer: Timer?
 var playerContainer = PlayerView()
 @IBOutlet weak var playheadSlider: UISlider!
 @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
 let SERVER_BASE_URL = "https://cdnapisec.kaltura.com"
 
 @objc var ks = "" {
    didSet {
      print("KSAQUI!!", ks)
    }
  }
 
 @objc var partner_id = "" {
    didSet {
      print("PARTNERID!!", partner_id)
      playLocal()
    }
  }

 @objc var entry_id = "" {
    didSet {
      print("ENTRY_ID!!", entry_id)
    }
  }
  
 override init(frame: CGRect) {
  super.init(frame: frame)
    self.frame = frame
    self.setupView()
 }
    
 required init?(coder aDecoder: NSCoder) {
   fatalError("init(coder:) has not been implemented")
 }
    
 private func setupView() {
  self.player = PlayKitManager.shared.loadPlayer(pluginConfig: nil)
  
  self.playerContainer = PlayerView()
  
  self.playerContainer.autoresizingMask = [.flexibleWidth, .flexibleHeight]
  
  self.playerContainer.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0)
  
  self.player?.view = self.playerContainer
    
  self.addSubview(playerContainer)
  self.bringSubviewToFront(playerContainer)
  
 }
    
 func playLocal() {
  let pId = (partner_id as NSString).integerValue

  print("chegou aqui", entry_id)
  print("chegou aqui", partner_id)
  print("chegou aqui", ks)

  let sessionProvider = SimpleSessionProvider(serverURL:SERVER_BASE_URL, partnerId: Int64(pId), ks:ks )
  let mediaProvider: OVPMediaProvider = OVPMediaProvider(sessionProvider)
  mediaProvider.entryId = entry_id
  mediaProvider.loadMedia { (mediaEntry, error) in
    print("MediaError", error)
    if let me = mediaEntry, error == nil {
        let mediaConfig = MediaConfig(mediaEntry: me, startTime: 0.0)
      self.player!.prepare(mediaConfig)
//            self.player!.play()
    } else {
      let alert = UIAlertView()
      alert.title = "Alert"
      alert.message = "Ks inválido"
      alert.addButton(withTitle: "OK")
      alert.show()
    }
  }
 }
  
 func preparePlayer(_ entry_id: String, ks: String?) {
  self.playerContainer = PlayerView()
  
  self.playerContainer.autoresizingMask = [.flexibleWidth, .flexibleHeight]
  
  self.playerContainer.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
  
  self.player?.view = self.playerContainer
  
  self.addSubview(playerContainer)
  self.bringSubviewToFront(playerContainer)
 }
  
 @IBAction func playTouched(_ sender: Any) {
  guard let player = self.player else {
  print("player is not set")
   return
  }
      
  if !(player.isPlaying) {
   player.play()
  }
 }
  
  @IBAction func pauseTouched(_ sender: Any) {
      guard let player = self.player else {
          print("player is not set")
          return
      }

      player.pause()
  }
  
  @IBAction func playheadValueChanged(_ sender: Any) {
      guard let player = self.player else {
          print("player is not set")
          return
      }
      
      let slider = sender as! UISlider
      
      print("playhead value:", slider.value)
      player.currentTime = player.duration * Double(slider.value)
  }
  
  @IBAction func replayTouched(_ sender: Any) {
      guard let player = self.player else {
          print("player is not set")
          return
      }
      
      player.replay()
  }
}

