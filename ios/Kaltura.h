//
//  Kaltura.h
//  Kaltura
//
//  Created by Fortal Sistemas on 23/01/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import "React/RCTBridgeModule.h"
#import "React/RCTViewManager.h"
#import "React/RCTUIManager.h"

@interface Kaltura: RCTViewManager

@property (nonatomic, assign) NSString *entry_id;
@property (nonatomic, assign) NSString *ks;
@property (nonatomic, assign) NSString *partner_id;

@end
