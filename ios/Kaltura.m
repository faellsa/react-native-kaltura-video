#import "Kaltura.h"

#import <Foundation/Foundation.h>

#import "React/RCTBridgeModule.h"
#import "React/RCTViewManager.h"
#import "React/RCTUIManager.h"

@interface RCT_EXTERN_MODULE(KalturaViewManager, RCTViewManager)

 RCT_EXPORT_VIEW_PROPERTY(entry_id, NSString)
 RCT_EXPORT_VIEW_PROPERTY(ks, NSString)
 RCT_EXPORT_VIEW_PROPERTY(partner_id, NSString)
 RCT_EXTERN_METHOD(pause)
 RCT_EXTERN_METHOD(getDuration:(RCTResponseSenderBlock)callback)
 RCT_EXTERN_METHOD(seekTo:(nonnull NSNumber *)time)
 RCT_EXTERN_METHOD(getCurrentPosition:(RCTResponseSenderBlock)callback)


@end
