//
//  KalturaViewManager.swift
//  Kaltura
//
//  Created by Fortal Sistemas on 23/01/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation
import PlayKit

@objc(KalturaViewManager)
class KalturaViewManager: RCTViewManager {
  var video: Player?
  
  
 override func view() -> UIView! {
  let view = KalturaPlayer()
  video = view.player!
  return view
 }
    
  @objc func pause() {
    if(video!.isPlaying){
      video?.pause()
    }else {
      video?.play()
    }
  }
  
  @objc func getDuration(_ callback: RCTResponseSenderBlock) -> Void {
    let resultsDict = video!.duration
    callback([resultsDict])
  }
  
  @objc func getCurrentPosition(_ callback: RCTResponseSenderBlock) -> Void {
    let resultsDict = video!.currentTime;
    callback([resultsDict])
  }
  
  @objc func seekTo(_ time: NSNumber) -> Void {
    let timeToSeek = TimeInterval(time)
    video?.seek(to: timeToSeek)
  }
  
 override static func requiresMainQueueSetup() -> Bool {
  return true
 }
    
    
}
