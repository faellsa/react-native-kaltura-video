import React from 'react';
import {
    requireNativeComponent, View, UIManager, Image, StyleSheet,
    findNodeHandle, TouchableOpacity, Text, NativeModules, Platform,
    TouchableWithoutFeedback, Dimensions
} from 'react-native';

import Slider from '@react-native-community/slider';
// import { Immersive } from 'react-native-immersive'
// import Orientation from 'react-native-orientation-locker';

const KalturaVideoView = requireNativeComponent("KalturaView");
const KalturaModule = NativeModules.KalturaViewManager;

const { height, width } = Dimensions.get('window');
export default class Video extends React.Component {
    state = {
        status: false,
        video: {},
        paused: true,
        showButtons: true,
        progress: 0,
        duration: -1,
        time: {
            min: 0,
            seg: 0
        },
        timeToSeek: Platform.OS === 'ios' ? 10 : 10000,
        timeFormated: "00:00",
        fullscreen: false
    }
    componentDidMount() {
        const { entry_id, ks } = this.props;
    }
    componentDidUpdate() {
        const { entry_id, ks } = this.props;

        if (entry_id) {
            if (this.state.duration <= 0) {
                KalturaModule.getDuration(
                    (res) => {
                        if (res !== "Erro") {
                            this.setState({ ...this.state, duration: res })
                        } else {
                            this.setState({ ...this.state, duration: -1 })
                        }
                    },
                );
            }
            if (this.state.duration >= 0 && !this.state.paused) {
                KalturaModule.getCurrentPosition(
                    (res) => {
                        var progress = (100 * res / this.state.duration) / 100
                        var min = parseInt(Platform.OS === 'ios' ? (res / 60) : (res / 60000));
                        var seg = parseInt(Platform.OS === 'ios' ? (res % 60000) - (min * 60) : (res % 60000) / 1000);
                        this.setState({
                            ...this.state, progress,
                            timeFormated: `${min < 10 ? `0${min}` : min}:${seg < 10 ? `0${seg}` : seg}`
                        })

                        if (res > this.state.duration) {
                            this.setState({ ...this.state, paused: true })
                        }
                    },
                );
            }
        }
    }

    // download = () => {
    //     const { entry_id, ks } = this.props;
    //     Platform.OS === 'ios' ?
    //         UIManager.dispatchViewManagerCommand(
    //             findNodeHandle(this.videoRef),
    //             UIManager["KalturaVideoView"].Commands.downloadVideo,
    //             [entry_id, ks]
    //         ) : KalturaModule.preparePlayer()
    // }

    play = () => {
        this.setPlayButton()
        KalturaModule.pause()
    }

    setPlayButton = () => {
        const { paused } = this.state
        if (paused) {
            this.setState({ ...this.state, paused: false })
            new Promise(resolve => {
                setTimeout(resolve, 500)
            }).then(() => this.setState({ ...this.state, showButtons: false }));
        } else {
            this.setState({ ...this.state, paused: true })
            new Promise(resolve => {
                setTimeout(resolve, 500)
            }).then(() => this.setState({ ...this.state, showButtons: true }));
        }
    }

    seekTo = (time) => {
        KalturaModule.getCurrentPosition(
            (res) => KalturaModule.seekTo(res + time),
        );
    }

    // setFull = () => {
    //     if (this.state.fullscreen) {
    //         if (Platform.OS === "android") {
    //             Immersive.off()
    //             Immersive.setImmersive(false)
    //         }
    //         Orientation.lockToPortrait()
    //     } else {
    //         if (Platform.OS === "android") {
    //             Immersive.on()
    //             Immersive.setImmersive(true)
    //         }
    //         Orientation.lockToLandscape()
    //     }
    //     this.setState({ ...this.state, fullscreen: !this.state.fullscreen })
    // }

    render() {
        const { entry_id, ks } = this.props;
        const { paused, showButtons, progress, timeFormated, timeToSeek, fullscreen } = this.state;
        return (
            entry_id ?
                <View>
                    <TouchableWithoutFeedback onPress={() => this.setState({ ...this.state, showButtons: true })}>
                        <KalturaVideoView
                            ref={e => this.videoRef = e}
                            ENTRY_ID={entry_id}
                            ks={ks}
                            downloaded={true}
                            style={{ width: '100%', height: fullscreen ? width : 300 }} />
                    </TouchableWithoutFeedback>
                    {showButtons ?
                        <View style={[styles.buttons, { height: fullscreen ? width : 300 }]}>
                            <TouchableOpacity onPress={() => this.seekTo(-(timeToSeek))}
                                style={{ marginTop: 20 }}>
                                <Image source={require('./rewind.png')} style={{ height: 30, width: 30 }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.play()}
                                style={{ marginTop: 20 }}>
                                {paused ?
                                    <Image source={require('./play.png')} style={{ height: 40, width: 40 }} />
                                    :
                                    <Image source={require('./pause.png')} style={{ height: 40, width: 40 }} />
                                }
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.seekTo(timeToSeek)}
                                style={{ marginTop: 20 }}>
                                <Image source={require('./forward.png')} style={{ height: 30, width: 30 }} />
                            </TouchableOpacity>
                        </View>
                        : null}
                    <View style={styles.bottom}>
                        <TouchableOpacity onPress={() => this.play()}>
                            {paused ?
                                <Image source={require('./play.png')} style={{ height: 20, width: 20 }} />
                                :
                                <Image source={require('./pause.png')} style={{ height: 20, width: 20 }} />
                            }
                        </TouchableOpacity>
                        <Slider
                            style={{ width: 200, height: 40 }}
                            minimumValue={0}
                            maximumValue={1}
                            step={0.001}
                            value={progress}
                            onSlidingStart={(e) => this.setState({ ...this.state, paused: true })}
                            onSlidingComplete={(e) => {
                                KalturaModule.seekTo(e * this.state.duration)
                                this.setState({ ...this.state, paused: false })
                            }}
                            minimumTrackTintColor="#FFFFFF"
                            maximumTrackTintColor="#000000"
                        />
                        <Text style={{ color: '#fff' }}>{timeFormated}</Text>
                        {/* <TouchableOpacity onPress={() => this.setFull()}>
                            <Image source={require('./fullscreen.png')} style={{ height: 20, width: 20 }} />
                        </TouchableOpacity> */}
                    </View>

                </View>
                : null
        );
    }
};

const styles = StyleSheet.create({
    buttons: {
        position: "absolute",
        width: '100%',
        backgroundColor: '#0000002b',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        flexDirection: 'row'
    },
    bottom: {
        height: 30,
        top: -30,
        width: '100%',
        backgroundColor: '#00000045',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
})